package com.example.debt.rearrangement.debtrearrangement.presenter

interface SigninResultCallBacks {
    fun onAuthSuccess(message: String)
    fun onAuthError(message: String)
}