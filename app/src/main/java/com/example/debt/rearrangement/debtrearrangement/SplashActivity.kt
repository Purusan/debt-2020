@file:Suppress("DEPRECATION")

package com.example.debt.rearrangement.debtrearrangement

import android.app.Service
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.debt.rearrangement.debtrearrangement.ui.MainActivity
import com.example.debt.rearrangement.debtrearrangement.ui.auth.SigninActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_splash.*
import java.lang.Exception

class SplashActivity : AppCompatActivity() {

    private var ms:Long = 0
    private var splashTimeOut:Long = 4000
    private var splashActive = true
    private var paused = false
    private var info: NetworkInfo? = null
    private var connectivityManager: ConnectivityManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val background = object : Thread() {
            override fun run() {
                super.run()
                try {
                    while (splashActive && ms<=splashTimeOut) {
                        if (!paused) {
                            ms += 1000
                        }
                        sleep(1000)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    if (!isConnected()) {
                        Snackbar.make(splashLayout, "No Internet Connection", Snackbar.LENGTH_INDEFINITE)
                            .setAction("Retry") {
                                recreate()
                            }
                            .show()
                    } else {
                        goToSignInActivity()
                    }
                }
            }
        }
        background.start()
    }

    private fun isConnected(): Boolean {
        connectivityManager = baseContext.getSystemService(Service.CONNECTIVITY_SERVICE) as ConnectivityManager
        info = connectivityManager!!.activeNetworkInfo
        return info != null && info!!.isConnectedOrConnecting
    }

    private fun goToSignInActivity() {
        val intent = Intent(baseContext, SigninActivity::class.java)
        startActivity(intent)
        finish()
    }
}
