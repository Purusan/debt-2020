package com.example.debt.rearrangement.debtrearrangement.model

import android.text.TextUtils
import android.util.Patterns
import androidx.databinding.BaseObservable
import java.util.regex.Pattern

class Users(private var email: String, private var password:String) : BaseObservable()

    val isDataValid: Boolean
    get() = (!TextUtils.isEmpty(getEmail()))
            && Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches()
            && getPassword().length >=6

    fun getEmail(): String {
        return email
    }

    fun getPassword(): String {
        return password
    }

